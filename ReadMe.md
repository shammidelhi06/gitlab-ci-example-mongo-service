# Mongodb gitlab-ci usage example

## Related documentation

* [Mongodb Data import documentation](https://docs.mongodb.com/getting-started/shell/import-data/)
* [How do I seed a mongodb database using docker-compose](https://stackoverflow.com/questions/31210973/how-do-i-seed-a-mongo-database-using-docker-compose)

## How to

### Seeding database for the first time

```bash
docker-compose up -d
docker-compose -f docker-compose.seed.yml up -d
```

or

```bash
docker-compose -f docker-compose.yml -f docker-compose.seed.yml up -d
```

### When already seeded

As usual, just run

```bash
docker-compose up -d
```

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)
